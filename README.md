**Welcome to my Individual Project**

Please contact prabhjotnainu12@gmail.com for any questions.

You will find the Python code which contains Streamlit, Plotly.express, and Pandas. To run the code you must have an python environment
with these libraries installed. 

You will also need to place the excel file in the correct directory with the code and also change the directory if needed. 
The change of directory will be line 12.

Thank you,
Prabhjot Nainu
