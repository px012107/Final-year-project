import os
import streamlit as st
import pandas as pd
import plotly_express as px

st.set_page_config(page_title="Module Evaluation Dashboard",
                   page_icon=":bar_chart:",
                   layout="wide")

# Load the Excel file into a DataFrame
df = pd.read_excel(
    io='Module_Evaluations_Data.xlsx',
    engine='openpyxl',
    sheet_name='Data',
    skiprows=0,
    usecols='A:R'
)

# ---- SIDEBAR ----

st.sidebar.header("Please Filter Here:")

default_level = [1, 2, 3]  # Specify a default value that is present in the options
level = st.sidebar.multiselect(
    "Select the level:",
    options=df["Level"].unique(),
    default=default_level
)

# Determine the minimum and maximum values from the "Average Score" column
min_average_score = df["Average Score"].min()
max_average_score = df["Average Score"].max()

# Use the minimum and maximum values from the column for the slider
average_score_range = st.sidebar.slider(
    "Select the Average Score Range:",
    min_value=min_average_score,
    max_value=max_average_score,
    value=(min_average_score, max_average_score)  # Default range from min to max
)

module_code = st.sidebar.multiselect(
    "Select the Module Code:",
    options=df["Module Code"].unique(),
    default=df["Module Code"].unique()
)

staff_default = df["Staff were good at explaining things"].unique()
module_org_default = df["The module was well organised"].unique()

# Remove NaN values from default lists
staff_default = [value for value in staff_default if pd.notna(value)]
module_org_default = [value for value in module_org_default if pd.notna(value)]

staff = st.sidebar.multiselect(
    "Select the Score for Staff:",
    options=df["Staff were good at explaining things"].unique(),
    default=staff_default
)

module_org = st.sidebar.multiselect(
    "Select the Score for Module Organization:",
    options=df["The module was well organised"].unique(),
    default=module_org_default
)

# Filters for additional evaluation criteria
blackboard_resources_default = df["The resources on Blackboard for this module supported my learning well"].unique()
diverse_content_default = df["The content of this module offered a diverse range of perspectives"].unique()
understood_requirements_default = df["I understood what was required of me to complete my assessment"].unique()
useful_feedback_default = df["I received useful feedback (consider both formal and informal feedback)"].unique()
practical_activities_default = df["Practical activities on this module helped me to learn"].unique()
opportunities_to_apply_default = df["This module provided me with opportunities to apply what I have learnt"].unique()
effective_communication_default = df["The requirements for assessment were communicated effectively"].unique()
sufficient_time_default = df["I invested enough time to meet the module requirements"].unique()

# Remove NaN values from default lists
blackboard_resources_default = [value for value in blackboard_resources_default if pd.notna(value)]
diverse_content_default = [value for value in diverse_content_default if pd.notna(value)]
understood_requirements_default = [value for value in understood_requirements_default if pd.notna(value)]
useful_feedback_default = [value for value in useful_feedback_default if pd.notna(value)]
practical_activities_default = [value for value in practical_activities_default if pd.notna(value)]
opportunities_to_apply_default = [value for value in opportunities_to_apply_default if pd.notna(value)]
effective_communication_default = [value for value in effective_communication_default if pd.notna(value)]
sufficient_time_default = [value for value in sufficient_time_default if pd.notna(value)]

blackboard_resources = st.sidebar.multiselect(
    "Resources on Blackboard supported my learning well:",
    options=df["The resources on Blackboard for this module supported my learning well"].unique(),
    default=blackboard_resources_default
)

diverse_content = st.sidebar.multiselect(
    "The content of this module offered a diverse range of perspectives:",
    options=df["The content of this module offered a diverse range of perspectives"].unique(),
    default=diverse_content_default
)

understood_requirements = st.sidebar.multiselect(
    "I understood what was required of me to complete my assessment:",
    options=df["I understood what was required of me to complete my assessment"].unique(),
    default=understood_requirements_default
)

useful_feedback = st.sidebar.multiselect(
    "I received useful feedback (consider both formal and informal feedback):",
    options=df["I received useful feedback (consider both formal and informal feedback)"].unique(),
    default=useful_feedback_default
)

practical_activities = st.sidebar.multiselect(
    "Practical activities on this module helped me to learn:",
    options=df["Practical activities on this module helped me to learn"].unique(),
    default=practical_activities_default
)

opportunities_to_apply = st.sidebar.multiselect(
    "This module provided me with opportunities to apply what I have learnt:",
    options=df["This module provided me with opportunities to apply what I have learnt"].unique(),
    default=opportunities_to_apply_default
)

effective_communication = st.sidebar.multiselect(
    "The requirements for assessment were communicated effectively:",
    options=df["The requirements for assessment were communicated effectively"].unique(),
    default=effective_communication_default
)

sufficient_time = st.sidebar.multiselect(
    "I invested enough time to meet the module requirements:",
    options=df["I invested enough time to meet the module requirements"].unique(),
    default=sufficient_time_default
)

df.columns = df.columns.str.replace(' ', '_')

df_selection = df.query(
    "Level == @level and @average_score_range[0] <= Average_Score <= @average_score_range[1] and Module_Code == @module_code and \
    `Staff_were_good_at_explaining_things` in @staff and \
    `The_module_was_well_organised` in @module_org and \
    `The_resources_on_Blackboard_for_this_module_supported_my_learning_well` in @blackboard_resources and \
    `The_content_of_this_module_offered_a_diverse_range_of_perspectives` in @diverse_content and \
    `I_understood_what_was_required_of_me_to_complete_my_assessment` in @understood_requirements and \
    `I_received_useful_feedback_(consider_both_formal_and_informal_feedback)` in @useful_feedback and \
    `Practical_activities_on_this_module_helped_me_to_learn` in @practical_activities and \
    `This_module_provided_me_with_opportunities_to_apply_what_I_have_learnt` in @opportunities_to_apply and \
    `The_requirements_for_assessment_were_communicated_effectively` in @effective_communication and \
    `I_invested_enough_time_to_meet_the_module_requirements` in @sufficient_time"
)

# Function to reset all filters
def reset_filters():
    level.clear()
    average_score_range = (min_average_score, max_average_score)  # Reassign default value
    module_code.clear()
    staff.clear()
    module_org.clear()
    blackboard_resources.clear()
    diverse_content.clear()
    understood_requirements.clear()
    useful_feedback.clear()
    practical_activities.clear()
    opportunities_to_apply.clear()
    effective_communication.clear()
    sufficient_time.clear()

# Add reset button to the sidebar
if st.sidebar.button("Reset Filters"):
    reset_filters()

#----Main Page-----

st.title(":bar_chart: Module Evaluation Dashboard")
st.markdown("##")

# Calculate the total number of participants
total_participants = len(df)

# Display the total number of participants in a text box
st.text(f"Total Participants: {total_participants}")

#----Visualsations----

# Calculate average score per module code
avg_score_per_module = df_selection.groupby('Module_Code')['Average_Score'].mean().reset_index()

# Create a bar chart using Plotly Express
bar_fig = px.bar(avg_score_per_module, x='Module_Code', y='Average_Score', title='Average Score per Module')

# Calculate the count of each level
level_counts = df['Level'].value_counts().reset_index()

# Rename columns for clarity
level_counts.columns = ['Level', 'Count']

# Create a pie chart using Plotly Express
pie_fig = px.pie(level_counts, values='Count', names='Level', title='Level Distribution')

# Calculate average score by level
avg_score_by_level = df.groupby('Level')['Average_Score'].mean().reset_index()

# Create a line graph using Plotly Express
line_fig = px.line(avg_score_by_level, x='Level', y='Average_Score', title='Average Score by Level')

# Calculate the count of each module code based on the selected filters
module_code_counts = df_selection['Module_Code'].value_counts().reset_index()

# Rename columns for clarity
module_code_counts.columns = ['Module_Code', 'Count']

# Create a bar chart using Plotly Express
module_code_bar_fig = px.bar(module_code_counts, x='Module_Code', y='Count', 
                             title='Number of Submissions per Module')

# Split the layout into two rows
row1, row2 = st.columns([1, 2])

# Display the bar chart in the first column of the first row
with row1:
    st.plotly_chart(bar_fig)

# Display the line graph in the second column of the first row
with row1:
    st.plotly_chart(line_fig)

# Display the pie chart in the second row
with row2:
    st.plotly_chart(pie_fig)

with row2:
    st.plotly_chart(module_code_bar_fig)
